package swagger

import (
	"encoding/base64"
	"encoding/json"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/filesystem"
	"github.com/markbates/pkger"
)

type Config struct {
	Dir           string
	IndexFile     string
	DocFile       string
	DocJsonString string
	DocSchema     map[string]interface{}
}

func New(c Config) fiber.Handler {
	if c.Dir == "" {
		c.Dir = "/docs"
	}
	c.createVirtualUI()
	return filesystem.New(filesystem.Config{
		Root: pkger.Dir(c.Dir),
	})
}

func (c Config) createVirtualUI() error {
	if err := pkger.MkdirAll(c.Dir, 0755); err != nil {
		return err
	}
	files := GetSwaggerUiFiles()
	for filename, base64_content := range files {
		content, err := base64.StdEncoding.DecodeString(base64_content)
		if err != nil {
			return err
		}
		if filename == "index.html" && c.IndexFile != "" {
			content, err = os.ReadFile(c.IndexFile)
			if err != nil {
				return err
			}
		}
		if err := CreateVirtualFile(c.Dir+"/"+filename, content); err != nil {
			return err
		}
	}

	var swagger_json []byte
	var err error
	if c.DocFile != "" {
		swagger_json, err = os.ReadFile(c.DocFile)
	} else if c.DocJsonString != "" {
		swagger_json = []byte(c.DocJsonString)
	} else if c.DocSchema != nil {
		swagger_json, err = json.Marshal(c.DocSchema)
		if err != nil {
			return err
		}
	}
	if err := CreateVirtualFile(c.Dir+"/swagger.json", swagger_json); err != nil {
		return err
	}
	return nil
}

func CreateVirtualFile(filename string, content []byte) error {
	f, err := pkger.Create(filename)
	if err != nil {
		return err
	}
	f.Write(content)
	if err := f.Close(); err != nil {
		return err
	}
	return nil
}
