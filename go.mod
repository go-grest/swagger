module grest.dev/swagger

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.23.0
	github.com/markbates/pkger v0.17.1
)
