package main

import (
	"github.com/gofiber/fiber/v2"
	"grest.dev/swagger"
)

func main() {
	app := fiber.New()
	app.Use("/docs/", swagger.New(swagger.Config{DocFile: "docs.json"}))
	app.Listen(":3000")
}
