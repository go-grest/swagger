module using-doc-file

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.23.0
	grest.dev/swagger v1.0.3
)
