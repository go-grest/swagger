package main

import (
	"github.com/gofiber/fiber/v2"
	"grest.dev/swagger"
)

func main() {
	app := fiber.New()
	app.Use("/docs/", swagger.New(swagger.Config{DocSchema: getSchema()}))
	app.Listen(":3000")
}

func getSchema() map[string]interface{} {
	return map[string]interface{}{
		"openapi": "3.0.0",
		"info": map[string]interface{}{
			"version":        "1.0.0",
			"title":          "Swagger Petstore Using DocSchema",
			"description":    "A sample API that uses a petstore as an example to demonstrate features in the OpenAPI 3.0 specification",
			"termsOfService": "http://swagger.io/terms/",
			"contact": map[string]interface{}{
				"name":  "Swagger API Team",
				"email": "apiteam@swagger.io",
				"url":   "http://swagger.io",
			},
			"license": map[string]interface{}{
				"name": "Apache 2.0",
				"url":  "https://www.apache.org/licenses/LICENSE-2.0.html",
			},
		},
		"servers": []interface{}{
			map[string]interface{}{
				"url": "http://petstore.swagger.io/api",
			},
		},
		"paths": map[string]interface{}{
			"/pets": map[string]interface{}{
				"get": map[string]interface{}{
					"description": "Returns all pets from the system that the user has access to\nNam sed condimentum est. Maecenas tempor sagittis sapien, nec rhoncus sem sagittis sit amet. Aenean at gravida augue, ac iaculis sem. Curabitur odio lorem, ornare eget elementum nec, cursus id lectus. Duis mi turpis, pulvinar ac eros ac, tincidunt varius justo. In hac habitasse platea dictumst. Integer at adipiscing ante, a sagittis ligula. Aenean pharetra tempor ante molestie imperdiet. Vivamus id aliquam diam. Cras quis velit non tortor eleifend sagittis. Praesent at enim pharetra urna volutpat venenatis eget eget mauris. In eleifend fermentum facilisis. Praesent enim enim, gravida ac sodales sed, placerat id erat. Suspendisse lacus dolor, consectetur non augue vel, vehicula interdum libero. Morbi euismod sagittis libero sed lacinia.\n\nSed tempus felis lobortis leo pulvinar rutrum. Nam mattis velit nisl, eu condimentum ligula luctus nec. Phasellus semper velit eget aliquet faucibus. In a mattis elit. Phasellus vel urna viverra, condimentum lorem id, rhoncus nibh. Ut pellentesque posuere elementum. Sed a varius odio. Morbi rhoncus ligula libero, vel eleifend nunc tristique vitae. Fusce et sem dui. Aenean nec scelerisque tortor. Fusce malesuada accumsan magna vel tempus. Quisque mollis felis eu dolor tristique, sit amet auctor felis gravida. Sed libero lorem, molestie sed nisl in, accumsan tempor nisi. Fusce sollicitudin massa ut lacinia mattis. Sed vel eleifend lorem. Pellentesque vitae felis pretium, pulvinar elit eu, euismod sapien.\n",
					"operationId": "findPets",
					"parameters": []interface{}{
						map[string]interface{}{
							"name":        "tags",
							"in":          "query",
							"description": "tags to filter by",
							"required":    false,
							"style":       "form",
							"schema": map[string]interface{}{
								"type": "array",
								"items": map[string]interface{}{
									"type": "string",
								},
							},
						},
						map[string]interface{}{
							"name":        "limit",
							"in":          "query",
							"description": "maximum number of results to return",
							"required":    false,
							"schema": map[string]interface{}{
								"type":   "integer",
								"format": "int32",
							},
						},
					},
					"responses": map[string]interface{}{
						"200": map[string]interface{}{
							"description": "pet response",
							"content": map[string]interface{}{
								"application/json": map[string]interface{}{
									"schema": map[string]interface{}{
										"type": "array",
										"items": map[string]interface{}{
											"$ref": "#/components/schemas/Pet",
										},
									},
								},
							},
						},
						"default": map[string]interface{}{
							"description": "unexpected error",
							"content": map[string]interface{}{
								"application/json": map[string]interface{}{
									"schema": map[string]interface{}{
										"$ref": "#/components/schemas/Error",
									},
								},
							},
						},
					},
				},
				"post": map[string]interface{}{
					"description": "Creates a new pet in the store. Duplicates are allowed",
					"operationId": "addPet",
					"requestBody": map[string]interface{}{
						"description": "Pet to add to the store",
						"required":    true,
						"content": map[string]interface{}{
							"application/json": map[string]interface{}{
								"schema": map[string]interface{}{
									"$ref": "#/components/schemas/NewPet",
								},
							},
						},
					},
					"responses": map[string]interface{}{
						"200": map[string]interface{}{
							"description": "pet response",
							"content": map[string]interface{}{
								"application/json": map[string]interface{}{
									"schema": map[string]interface{}{
										"$ref": "#/components/schemas/Pet",
									},
								},
							},
						},
						"default": map[string]interface{}{
							"description": "unexpected error",
							"content": map[string]interface{}{
								"application/json": map[string]interface{}{
									"schema": map[string]interface{}{
										"$ref": "#/components/schemas/Error",
									},
								},
							},
						},
					},
				},
			},
			"/pets/{id}": map[string]interface{}{
				"get": map[string]interface{}{
					"description": "Returns a user based on a single ID, if the user does not have access to the pet",
					"operationId": "find pet by id",
					"parameters": []interface{}{
						map[string]interface{}{
							"name":        "id",
							"in":          "path",
							"description": "ID of pet to fetch",
							"required":    true,
							"schema": map[string]interface{}{
								"type":   "integer",
								"format": "int64",
							},
						},
					},
					"responses": map[string]interface{}{
						"200": map[string]interface{}{
							"description": "pet response",
							"content": map[string]interface{}{
								"application/json": map[string]interface{}{
									"schema": map[string]interface{}{
										"$ref": "#/components/schemas/Pet",
									},
								},
							},
						},
						"default": map[string]interface{}{
							"description": "unexpected error",
							"content": map[string]interface{}{
								"application/json": map[string]interface{}{
									"schema": map[string]interface{}{
										"$ref": "#/components/schemas/Error",
									},
								},
							},
						},
					},
				},
				"delete": map[string]interface{}{
					"description": "deletes a single pet based on the ID supplied",
					"operationId": "deletePet",
					"parameters": []interface{}{
						map[string]interface{}{
							"name":        "id",
							"in":          "path",
							"description": "ID of pet to delete",
							"required":    true,
							"schema": map[string]interface{}{
								"type":   "integer",
								"format": "int64",
							},
						},
					},
					"responses": map[string]interface{}{
						"204": map[string]interface{}{
							"description": "pet deleted",
						},
						"default": map[string]interface{}{
							"description": "unexpected error",
							"content": map[string]interface{}{
								"application/json": map[string]interface{}{
									"schema": map[string]interface{}{
										"$ref": "#/components/schemas/Error",
									},
								},
							},
						},
					},
				},
			},
		},
		"components": map[string]interface{}{
			"schemas": map[string]interface{}{
				"Pet": map[string]interface{}{
					"allOf": []interface{}{
						map[string]interface{}{
							"$ref": "#/components/schemas/NewPet",
						},
						map[string]interface{}{
							"type": "object",
							"required": []interface{}{
								"id",
							},
							"properties": map[string]interface{}{
								"id": map[string]interface{}{
									"type":   "integer",
									"format": "int64",
								},
							},
						},
					},
				},
				"NewPet": map[string]interface{}{
					"type": "object",
					"required": []interface{}{
						"name",
					},
					"properties": map[string]interface{}{
						"name": map[string]interface{}{
							"type": "string",
						},
						"tag": map[string]interface{}{
							"type": "string",
						},
					},
				},
				"Error": map[string]interface{}{
					"type": "object",
					"required": []interface{}{
						"code",
						"message",
					},
					"properties": map[string]interface{}{
						"code": map[string]interface{}{
							"type":   "integer",
							"format": "int32",
						},
						"message": map[string]interface{}{
							"type": "string",
						},
					},
				},
			},
		},
	}
}
