package swagger

import (
	"encoding/json"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

func TestDocFile(t *testing.T) {
	app := fiber.New()
	app.Use("/docs/", New(Config{DocFile: "example/using-doc-file/docs.json"}))

	resp, err := app.Test(httptest.NewRequest("GET", "/docs/", nil))
	utils.AssertEqual(t, nil, err)
	utils.AssertEqual(t, 200, resp.StatusCode)
}

func TestDocJsonString(t *testing.T) {
	swagger_json, _ := os.ReadFile("example/using-doc-file/docs.json")

	app := fiber.New()
	app.Use("/docs/", New(Config{DocJsonString: string(swagger_json)}))

	resp, err := app.Test(httptest.NewRequest("GET", "/docs/", nil))
	utils.AssertEqual(t, nil, err)
	utils.AssertEqual(t, 200, resp.StatusCode)
}

func TestDocSchema(t *testing.T) {
	var schema map[string]interface{}
	swagger_json, _ := os.ReadFile("example/using-doc-file/docs.json")
	json.Unmarshal(swagger_json, &schema)

	app := fiber.New()
	app.Use("/docs/", New(Config{DocSchema: schema}))

	resp, err := app.Test(httptest.NewRequest("GET", "/docs/", nil))
	utils.AssertEqual(t, nil, err)
	utils.AssertEqual(t, 200, resp.StatusCode)
}
